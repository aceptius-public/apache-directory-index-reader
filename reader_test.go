package apache_directory_index_reader

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"os"
	"reflect"
	"testing"
)

// MockClient is the mock client
type MockClient struct {
	DoFunc func(req *http.Request) (*http.Response, error)
}

// Do is the mock client's `Do` func
func (m *MockClient) Do(req *http.Request) (*http.Response, error) {
	return m.DoFunc(req)
}

func TestReader_GetList(t *testing.T) {
	type fields struct {
		client     Doer
		listParser ListParser
	}
	type args struct {
		ctx context.Context
		uri string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    FileList
		wantErr bool
	}{
		{
			name: "test-parsing",
			fields: fields{
				client: &MockClient{
					DoFunc: func(req *http.Request) (*http.Response, error) {
						if req.URL.Path != "/test-data" {
							t.Errorf("Expected to request '/test-data', got: %s", req.URL.Path)
						}
						if req.Header.Get("Accept") != "text/html" {
							t.Errorf("Expected Accept: text/html header, got: %s", req.Header.Get("Accept"))
						}

						html, err := os.ReadFile("./fixtures/test_page.html")
						if err != nil {
							t.Errorf("error reading fixture file: %s", err)
						}

						return &http.Response{
							StatusCode: 200,
							Body:       io.NopCloser(bytes.NewBuffer(html)),
						}, nil
					},
				},
				listParser: &DefaultListParser{},
			},
			args: args{
				ctx: context.Background(),
				uri: "/test-data",
			},
			want: FileList{
				{
					Link: "1.zip",
					Name: "1.zip",
				},
				{
					Link: "2.zip",
					Name: "2.zip",
				},
				{
					Link: "3.zip",
					Name: "3.zip",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := Reader{
				client:     tt.fields.client,
				listParser: tt.fields.listParser,
			}
			got, err := r.GetList(tt.args.ctx, tt.args.uri)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetList() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetList() got = %v, want %v", got, tt.want)
			}
		})
	}
}
