# Apache Directory Index Reader

Read Apache file browser page and parse info structs

```go
package main

import (
	"context"
	"fmt"

	ar "gitlab.com/aceptius-public/apache-directory-index-reader"
)

func main() {
	reader := ar.DefaultReader()
	files, err := reader.GetList(context.Background(), "https://ftp.fau.de/kiwix/zim/wikipedia/")
	if err!=nil {
		panic(err)
	}

	fmt.Printf("%+v", files)
}
```