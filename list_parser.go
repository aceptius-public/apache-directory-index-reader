package apache_directory_index_reader

import (
	"context"
	"fmt"
	"io"

	"github.com/PuerkitoBio/goquery"
)

type DefaultListParser struct {
}

func (d DefaultListParser) ParseList(_ context.Context, reader io.Reader) (FileList, error) {
	doc, err := goquery.NewDocumentFromReader(reader)
	if err != nil {
		return nil, fmt.Errorf("error while reading body: %w", err)
	}

	out := FileList{}
	// Find the review items
	doc.Find("pre a").Each(func(i int, s *goquery.Selection) {
		// first 5 links are apache page controls
		if i <= 4 {
			return
		}

		h, ok := s.Attr("href")
		if !ok {
			return
		}

		out = append(out, FileListItem{
			Link: h,
			Name: s.Text(),
		})
	})

	return out, nil
}
