package apache_directory_index_reader

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"
)

type FileListItem struct {
	Link string
	Name string
}

type FileList []FileListItem

type Doer interface {
	Do(*http.Request) (*http.Response, error)
}

type ListParser interface {
	ParseList(ctx context.Context, reader io.Reader) (FileList, error)
}

func DefaultReader() *Reader {
	return &Reader{
		client: &http.Client{
			Timeout: 60 * time.Second,
		},
		listParser: &DefaultListParser{},
	}
}

type Reader struct {
	client     Doer
	listParser ListParser
}

var ErrWrongResponseCode = errors.New("invalid response code")
var ErrNotFoundCode = errors.New("not found response code")

// GetList returns list of files from apache list index. Returns error if something bad happen.
// Returns specific ErrWrongResponseCode response if server response with other than 200 code.
func (r Reader) GetList(ctx context.Context, uri string) (FileList, error) {
	if r.listParser == nil {
		return nil, errors.New("no parser available")
	}

	if r.client == nil {
		return nil, errors.New("no doer available for handling request")
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, uri, nil)
	if err != nil {
		return nil, fmt.Errorf("error creating request: %w", err)
	}

	req.Header.Add("Accept", "text/html")

	resp, err := r.client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error in request[%s]: %w", req.RequestURI, err)
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode == http.StatusNotFound {
		return nil, ErrNotFoundCode
	}

	if resp.StatusCode != http.StatusOK {
		return nil, ErrWrongResponseCode
	}

	return r.listParser.ParseList(ctx, resp.Body)
}
